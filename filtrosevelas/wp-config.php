<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'filtrosevelas');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oH]_h`0YV-7pqf88!Br1$3+S.q-KpR= oE=/pCPgMpoM.0x3l&.WkjiE2M1+4_(G');
define('SECURE_AUTH_KEY',  '(AY6H@exCj?*6JT7GRy&@VK,S3]`bE8%vA897*QF.,psVad7rkb^Mse_8UeOIR8/');
define('LOGGED_IN_KEY',    'U,a=5^/RhwQc]v5|qppz?$y=1^^:LMg_Ec4bQ`{Xy<u).; 1?:C-?$p)2)47iA T');
define('NONCE_KEY',        'tjod-eNy&ACWwtgJ-`GC7`=8FT|rYp^kJ[K|KC_Q%WZi*0oSIuIMp}jnAN^$/m@C');
define('AUTH_SALT',        'Sd-]VlFlS]#D}eu y.qfwN5l!dU*?052y>o2TJD.1g4r{90  dEE7sXw^p0dN.(m');
define('SECURE_AUTH_SALT', 'F`>b?|1oz+pRV7e aU>t#aer. 5MB>5;( 1k `!s~(c=CkDb*Ng]C?bEMmUJcFeJ');
define('LOGGED_IN_SALT',   '<:v)fm&9w/y;++s/WHAwIL^R&,H(4|R2|{5sEqJYb{iCE+>_G0P4xx8GDd9DzPP_');
define('NONCE_SALT',       'j K}-%n+T1g@qjB,Ft2XE0OmbB%Y?<=x&d(5& B3-.}Vbz|n7t43/lkJ7D.pT;j%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'fv_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
